GPG_KEY_ID ?= 73880ECAF69EC2ED44CE5889502BFB12D0B5295F
.DEFAULT_GOAL := run


clean:
	rm -rf build
.PHONY: clean


clean_sig:
	rm -f static/**.asc
.PHONY: clean_sig


%.asc:
	gpg --sign --yes \
		-u ${GPG_KEY_ID} --digest-algo SHA512 \
		-o $*.asc \
		--clear-sign $*


static/.well-known/opengpgkey:
	gpg --export --armour  --digest-algo SHA512 \
		 -o static/.well-known/opengpgkey \
		 ${GPG_KEY_ID}
.PHONY: static/.well-known/opengpgkey


static/.well-known/security.txt:
	gpg --sign --yes \
		-u ${GPG_KEY_ID} --digest-algo SHA512 \
		-o static/.well-known/security.txt \
		--clear-sign static/.well-known/security.clear.txt
.PHONY: static/.well-known/security.txt


gpg: clean_sig \
	static/contact.html.asc \
	static/.well-known/security.txt \
	static/.well-known/opengpgkey
.PHONY: sign


build:
	mkdir -p build/css
	cp -a ./static/* build/
	cp -a ./static/.* build/
	sass ./sass/style.scss ./build/css/style.css --style compressed
.PHONY: build


build_watch:
	find . -type f | entr $(MAKE) build
.PHONY: build_watch


run: build
	wrangler pages dev build
.PHONY: serve


upload: build
	wrangler pages deploy build --project-name homepage
.PHONY: upload

